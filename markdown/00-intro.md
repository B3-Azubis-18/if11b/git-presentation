<!-- .slide: data-state="cover" id="cover-page" data-timing="20" data-menu-title="Cover slide" -->
<div class="title">
    <h1>Git</h1>
    <h2>und Versionskontrollsysteme</h2>
</div>

<div class="row presenters">
    <div class="presenter presenter-1">
        <h3 class="name">Enno Gotthold</h3>
        <h3 class="job-title">Azubi</h3>
        <h3 class="email"><a href="mailto:enno.gotthold@suse.de">enno.gotthold@suse.de</a></h3>
    </div>
</div>


<!-- .slide: data-state="normal" id="agenda" data-menu-title="Agenda" -->
## Agenda

<h3>
Willkommen zu meiner Präsentation mithilfe von [`reveal.js`](https://github.com/hakimel/reveal.js/) über Git & Co.
</h3>

Wir haben die Folgende Agenda:

*   Einführung
*   Was ist ein VCS und wieso wollt ihr es?
*   Live-Demo
*   Zusammenfassung
