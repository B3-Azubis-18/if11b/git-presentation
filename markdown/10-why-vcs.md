<!-- .slide: data-state="normal" id="why-a-vcs-intro" data-menu-title="Warum ein VCS?" -->
## Warum ein VCS?

<div class="center-container">
    <img class="item-container" data-src="images/mindstorms.jpeg" style="height: 100%" />
</div>


<!-- .slide: data-state="normal" id="why-a-vcs-pain" data-menu-title="Der Schmerz" -->
## Der Schmerz ohne VCS!

<div class="center-container">
    <img class="item-container" data-src="images/crying_emoji.png" style="height: 100%" />
</div>


<!-- .slide: data-state="normal" id="why-a-vcs-pros" data-menu-title="Die Vorteile" -->
## Die Vorteile

- Kollaboration
- Versionskontrolle
- Zurückrollen von alten Versionen
- Datei/Projekthistorie
- Backup
- Reproduzierbarkeit

Note: Source for this: https://www.git-tower.com/learn/git/ebook/en/command-line/basics/why-use-version-control
