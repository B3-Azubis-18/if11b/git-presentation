<!-- .slide: data-state="normal" id="git" data-menu-title="Git" -->
## Die Rettung

<div class="center-container">
    <img class="item-container" data-src="images/git_logo.png" style="height: 100%" />
</div>


<!-- .slide: data-state="normal" id="git-history" data-menu-title="Geschichte" -->
## Die Geschichte von Git

- Anfang 2005: BitKeeper ändert seine Lizenz
- 3\. April 2005: Beginn der Entwicklung von Git
- 6\. April 2005: Ankündigung des Projektes
- 7\. April 2005: Entwicklung von Git mithilfe von Git
- 29\. April 2005: Git bricht Rekorde für wichtige Funktionen von VCS
- 16\. Juni 2005: Git managed den Linux-Kernel-Release 2.6.12
- 26\. Juli 2005: Junio Hamano wird Git Maintainer
- 21\. Dezember 2005: Git Release 1.0
- 28\. Mai 2013: Git Relase 2.0
- 16\. August 2019: Aktuelles Git Release 2.23

Note: Quelle: https://en.wikipedia.org/wiki/Git#History
