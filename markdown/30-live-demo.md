<!-- .slide: data-state="section-break" id="live-demo" data-menu-title="Live-Demo" -->
## Live-Demo


<!-- .slide: data-state="normal" id="live-demo-1" data-menu-title="Projekterstellung" -->
## Projekt auf Gitlab erstellen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/01-create-project-on-gitlab.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-2" data-menu-title="Repoerstellung" -->
## Projekt auf Gitlab erstellen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/02-create-project-on-gitlab.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-2" data-menu-title="Repoerstellung" -->
## Repo erstellen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/03-clone-project.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-3" data-menu-title="Commit erstellen" -->
## Etwas in das Repo committen & hochladen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/04-make-changes.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-6" data-menu-title="Branch erstellen" -->
## Branch erstellen & hochladen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/05-branching.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-9" data-menu-title="Merge-Request erstellen 1" -->
## Merge-Request erstellen 1

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/06-merge-request-1.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-10" data-menu-title="Merge-Request erstellen 2" -->
## Merge-Request erstellen 2

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/07-merge-request-2.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-11" data-menu-title="Mergen" -->
## Mergen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/08-merge-request-3.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-12" data-menu-title="Git Log" -->
## Änderungen herunterladen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/09-änderungen-ziehen.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-13" data-menu-title="Zusammenfassung" -->
## Änderungen überprüfen

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/10-änderungen-überprüfen.png" />
</div>


<!-- .slide: data-state="normal" id="live-demo-14" data-menu-title="Zusammenfassung" -->
## Git Log

<div class="center-container">
    <img class="item-container" data-src="images/live-demo/11-git-log.png" />
</div>


<!-- .slide: data-state="normal" id="git-command-list" data-menu-title="Git Kommando Liste" -->
## Kommandoliste

````bash
git clone <URL>
git add <FILE>
git status
git config [--global] <KEY> <VALUE>
git commit -m "Message"
git remote -v
git push <BRANCH>
git pull
git diff
git checkout -b <BRANCHNAME>
````


<!-- .slide: data-state="normal" id="live-demo-15" data-menu-title="Zusammenfassung" -->
## Zusammenfassung

*   Repository erstellen auf gitlab
*   Mit Git entwickeln
*   Änderungen verfolgen
*   Änderungen wieder hochladen
